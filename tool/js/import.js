htmlKit = {
    _tags: [],
    html: [],
    _createAttrs: function(attrs) {
        var attrStr = [];
        for (var key in attrs) {
            if (!attrs.hasOwnProperty(key)) continue;
            attrStr.push(key + "=" + attrs[key] + "")
        }
        return attrStr.join(" ")
    },
    _createTag: function(tag, attrs, isStart) {
        if (isStart) {
            return "<" + tag + " " + this._createAttrs(attrs) + ">"
        } else {
            return "</" + tag + ">"
        }
    },
    start: function(tag, attrs) {
        this._tags.push(tag);
        this.html.push(this._createTag(tag, attrs, true))
    },
    end: function() {
        this.html.push(this._createTag(this._tags.pop(), null, false))
    },
    tag: function(tag, attr, text) {
        this.html.push(this._createTag(tag, attr, true) + text + this._createTag(tag, null, false))
    },
    create: function() {
        return this.html.join("")
    }
};

function json2Html(data) {
    console.log('data', data)
    var hk = htmlKit;
    hk.html = [];
    hk.start("table", { "id": "test", "cellpadding": "10", "cellspacing": "0", "border": "1" });
    hk.start("thead");
    hk.start("tr");
    hk.end();
    hk.end();
    hk.start("tbody");
    for (var i = data.length - 1; i >= 0; i--) {
        if (data[i].经度 == "" && data[i].纬度 == "" && data[i].面积 == "") {
            data.splice(i, 1);
        }
    }
    $.each(data, function(k, v) {
        if (k == 0) {
            hk.start("tr");
            $.each(v, function(key, val) {
                hk.tag("td", null, key)
            });
            hk.end()
        }
        hk.start("tr");
        $.each(v, function(key, val) {
            hk.tag("td", null, val)
        });
        hk.end()
    });
    hk.end();
    hk.end();
    // $('.open').html("");
    $('.open').html(hk.create());
    return hk.create()
}